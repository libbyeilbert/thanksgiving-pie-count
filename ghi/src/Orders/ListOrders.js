import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { DateTime } from 'luxon';
import styles from './orders.module.css';

function ListOrders() {
    const { date } = useParams();
    const [orders, setOrders] = useState([]);
    const [items, setItems] = useState([]);
    const [counts, setCounts] = useState([]);



    const dt = DateTime.fromISO(date, {zone: 'utc'});
    const formattedDate = dt.setZone('utc').toFormat('ccc, M/d');

    const fetchOrders = async () => {
        const url = `http://localhost:8080/orders/list/${date}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setOrders(data);
        };
    };

    const fetchItems = async () => {
        const url = `http://localhost:8080/orders/items/count/${date}`;
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setItems(data);
        };
    };

    const fetchCounts = async () => {
        const url = `http://localhost:8080/fourpacks/${date}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCounts(data);
        };
    };

    const fetchPendingCounts = async () => {
        const url = `http://localhost:8080/pending/${date}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setItems(data);
        };
    };

    const deleteOrder = (orderId) => {
        const url = `http://localhost:8080/orders/${orderId}`;
        fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(response => {
        if (response.ok) {
            fetchOrders();
            };
        });
    };

useEffect(() => {
    fetchOrders(date);
    fetchItems();
    fetchCounts();
}, [date]);

return (
    <div className={styles.container}>

    <div>
        <h1>{formattedDate}</h1>
    <table className={styles.table}>
        <tbody>
            {orders.map(order => {
                const url = `http://localhost:3000/${order._id}`
                return (
                    <tr key={order._id}>
                        <td className={styles.orderstabledata}>
                            <a href={url}>{order.name}</a>
                        </td>
                        <td className={styles.orderstabledata}>
                            {order.displayTime}
                        </td>
                        <td className={styles.orderstabledata}>
                            {order.status}
                        </td>
                        <td className={styles.orderstabledata}>
                            <button onClick={() => deleteOrder(order._id)}>Delete</button>
                        </td>
                    </tr>
                )
            })}
        </tbody>
    </table>
    </div>
    <div className={styles.rightcontainer}>
        <button onClick={fetchPendingCounts}>Show pending only</button>
    <table className={styles.table}>
            <thead>
                <tr>
                    <th className={styles.tabledata}>Product</th>
                    <th className={styles.tabledata}>Count</th>
                </tr>
            </thead>
            <tbody>
                {items.map(item => {
                    return (
                        <tr key={item._id}>
                            <td className={styles.tabledata}>{item._id}</td>
                            <td className={styles.tabledata}>{item.count}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    <div className={styles.rightcontainer}>
            <table className={styles.table}>
                <thead>
                    <tr>
                        <th className={styles.tabledata}>Fourpack Flavor</th>
                        <th  className={styles.tabledata}>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {counts.map(flavor => {
                        return (
                            <tr key={flavor._id}>
                                <td className={styles.tabledata}>{flavor._id}</td>
                                <td className={styles.tabledata}>{flavor.count}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            </div>
    </div>
)

};

export default ListOrders;
